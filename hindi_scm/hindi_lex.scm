;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                             ;;
;;;  Lexical Analysis: lexical lookup, letter-to-sound rules (words to phones)  ;;
;;;                                                                             ;;
;;;  Copyright (c) 2005, Chaitanya Kamisetty <chaitanya@atc.tcs.co.in>          ;;
;;;  										;;
;;;  Copyright (c) 2006, Priti Patil, janabhaaratii, C-DAC, Mumbai              ;;
;;;                     <prithisd@cdacmumbai.in>, <prithisd@gmail.com>          ;;
;;;  										;;
;;;  This program is a part of festival-hi.					;;
;;;  										;;
;;;  festival-hi is free software; you can redistribute it and/or modify        ;;
;;;  it under the terms of the GNU General Public License as published by	;;
;;;  the Free Software Foundation; either version 2 of the License, or		;;
;;;  (at your option) any later version.					;;
;;;										;;
;;;  This program is distributed in the hope that it will be useful,		;;
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of		;;
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		;;
;;;  GNU General Public License for more details.				;;
;;;										;;
;;;  You should have received a copy of the GNU General Public License		;;
;;;  along with this program; if not, write to the Free Software		;;
;;;  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA  ;;
;;;										;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (hindi_addenda)
  "(hindi_addenda)
Basic lexicon should (must ?) basic letters and punctuation."
;; Basic punctuation must be in with nil pronunciation
(lex.add.entry '("'" punc nil))
(lex.add.entry '(":" punc nil))
(lex.add.entry '(";" punc nil))
(lex.add.entry '("," punc nil))
(lex.add.entry '("." punc nil))
(lex.add.entry '("-" punc nil))
(lex.add.entry '("\"" punc nil))
(lex.add.entry '("`" punc nil))
(lex.add.entry '("?" punc nil))
(lex.add.entry '("!" punc nil))
(lex.add.entry '("(" punc nil))
(lex.add.entry '(")" punc nil))
(lex.add.entry '("{" punc nil))
(lex.add.entry '("}" punc nil))
(lex.add.entry '("[" punc nil))
(lex.add.entry '("]" punc nil))
)

(lts.ruleset
 hindi
  ( 
	; Matras can be formed by (OCT340 OCT244 MAT1 )  and (OCT340 OCT245 MAT2)
	( OCT340 � ) 
	( OCT244 � )
	( OCT245 � )
	( MAT1 � � )
	( MAT2 � � � � � � � � � � � � � � � � � � � � )
  )
  (
;; single vowels
( [ � � � ] = a ) ;hindi letter A 
( [ � � � ] = aa ) ;hindi letter AA
( [ � � � ] = ih ) ;hindi letter I
( [ � � � ] = iy ) ;hindi letter II
( [ � � � ] = uh ) ;hindi letter U
( [ � � � ] = uw ) ;hindi letter UU 
( [ � � � ] = r uh ) ;hindi letter vocalic R 
( [ � � � ]  = r uw ) ;hindi letter vocalic RR 
([ � � � ] =  l uh ) ; hindi letter vocalic L (using 'lu' sound)
([ � � � ] = l uw ) ; hindi letter vocalic LL (uinsg 'luu' sound)
( [ � � � ] = eh ) ;U+090D hindi letter chandra E
( [ � � �  ] = ay ) ;U+090E DEVANAGARI LETTER SHORT E (for transcribing Dravidian short e)
( [ � � � ] = eh ) ;hindi letter E
;( [ � � � ] = ee ) ;no hindi letter EE
( [ � � � ] = ay ) ;hindi letter AI
( [ � � � ] = oo ) ;U+0911 DEVANAGARI LETTER CANDRA O
( [ � � � ] = oh )  ;hindi letter O 
( [ � � � ] = oh )  ;U+0912 DEVANAGARI LETTER SHORT O 
( [ � � � ] = aw ) ;hindi letter AU
( [ � � � ]  = m ) ;hindi sign anusvara (sunna) (using 'ma' sound)
([ � � � ] = h a ) ;hindi sign visarga (using 'ha' sound)
;; consonants in half forms i.e. consonant + halant
( [ � � � � � � ]  = k ) ;hindi letter K
( [ � � � � � � ]  = k ) ;U+0958 DEVANAGARI LETTER QA ( U+0915 DEVANAGARI LETTER KA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = kh ) ;hindi letter KH
( [ � � � � � � ]  = kh ) ;U+0959 DEVANAGARI LETTER KHHA (U+0916 DEVANAGARI LETTER KHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = g ) ;hindi letter G
( [ � � � � � � ]  = g ) ;U+095A DEVANAGARI LETTER GHHA (U+0917 DEVANAGARI LETTER GA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = g h ) ;hindi letter GH
( [ � � � � � � ] = n y ) ;hindi letter NG
( [ � � � � � � ] = ch ) ;hindi letter C
( [ � � � � � � ] = ch h )	;hindi letter CH 
( [ � � � � � � ] = j ) ;hindi letter J
( [ � � � � � � ]  = j ) ;U+095B DEVANAGARI LETTER ZA ( U+091C DEVANAGARI LETTER JA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = j h )	;hindi letter JH (missing in phoneset)
( [ � � � � � � ] = eh n eh )	;hindi letter NY (missing in phoneset)
( [ � � � � � � ] = T ) ;hindi letter TT
( [ � � � � � � ] = T h )	;hindi letter TTH (missing in phoneset)
( [ � � � � � � ] = D ) ;hindi letter DD
( [ � � � � � � ]  = D ) ;U+095C DEVANAGARI LETTER DDDHA (U+0921 DEVANAGARI LETTER DDA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = D h ) ;hindi letter DDH (missing in phoneset)
( [ � � � � � � ]  = D h) ;U+095D DEVANAGARI LETTER RHA (U+0922 DEVANAGARI LETTER DDHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = N ) ;hindi letter NN (missing in phoneset)
( [ � � � � � � ] = th )	;hindi letter T (missing in phoneset)
( [ � � � � � � ] = tth ) ;hindi letter TH
( [ � � � � � � ] = dh )	;hindi letter D (missing in phoneset)
( [ � � � � � � ] = ddh )	;hindi letter DH
( [ � � � � � � ] = n ) ;hindi letter N
( [ � � � � � � ] = n ) ;half hindi letter NNNA (U+0928 DEVANAGARI LETTER NA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = p ) ;hindi letter P
( [ � � � � � � ] = f ) ;hindi letter PH
( [ � � � � � � ]  = f ) ;U+095E DEVANAGARI LETTER FA (U+092B DEVANAGARI LETTER PHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = b ) ;hindi letter B
( [ � � � � � � ] = bh ) ;hindi letter BH
( [ � � � � � � ] = m ) ;hindi letter M
( [ � � � � � � ] = y ) ;hindi letter Y
( [ � � � � � � ]  = y ) ;U+095F DEVANAGARI LETTER YYA (U+092F DEVANAGARI LETTER YA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � � � � ] = r ) ;hindi letter R
( [ � � � � � � ] = r ) ;half hindi letter U+0931 RRA ( U+0930 DEVANAGARI LETTER RA + U+093C DEVANAGARI SIGN NUKTA)  for transcribing Dravidian alveolar r and half form is represented as "Eyelash RA"
( [ � � � � � � ] = l ) ;hindi letter L 
( [ � � � � � � ] = L ) ;hindi letter LL
( [ � � � � � � ] = L ) ;half hindi letter LLLA, U+0934 DEVANAGARI LETTER LLLA (U+0933 DEVANAGARI LETTER LLA + U+093C DEVANAGARI SIGN NUKTA) for transcribing Dravidian l
( [ � � � � � � ] = v ) ;hindi letter V
( [ � � � � � � ] = sh ) ;hindi letter SH
( [ � � � � � � ] = sh h ) ;hindi letter SS
( [ � � � � � � ] = s ) ;hindi letter S (missing in phoneset)
( [ � � � � � � ] = h ) ;hindi letter H
;;consonants occuring as vattulu
;; matches the regex [consonant]{consonant}
( [ � � � ]  OCT340 OCT244 MAT1  =  k ) ;hindi letter K
( [ � � � ]  OCT340 OCT244 MAT1  =  k ) ;hindi letter K U+0958 DEVANAGARI LETTER QA ( U+0915 DEVANAGARI LETTER KA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]  OCT340 OCT244 MAT1 = kh ) ;hindi letter KH
( [ � � � ]  OCT340 OCT244 MAT1 = kh ) ;hindi letter KH U+0959 DEVANAGARI LETTER KHHA (U+0916 DEVANAGARI LETTER KHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT244 MAT1 = g ) ;hindi letter G
( [ � � � ]   OCT340 OCT244 MAT1 = g ) ;hindi letter G U+095A DEVANAGARI LETTER GHHA (U+0917 DEVANAGARI LETTER GA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT244 MAT1 = g h ) ;hindi letter GH
( [ � � � ]  OCT340 OCT244 MAT1 = n y ) ;hindi letter NG
( [ � � � ]   OCT340 OCT244 MAT1 = ch ) ;hindi letter C
( [ � � � ]  OCT340 OCT244 MAT1  = ch h ) ;hindi letter CH
( [ � � � ]  OCT340 OCT244 MAT1  = j ) ;hindi letter J
( [ � � � ]  OCT340 OCT244 MAT1  = j ) ;hindi letter J U+095B DEVANAGARI LETTER ZA ( U+091C DEVANAGARI LETTER JA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT244 MAT1 = j h ) ;hindi letter JH
( [ � � � ]   OCT340 OCT244 MAT1 = eh n eh ) ;hindi letter NY
( [ � � � ]   OCT340 OCT244 MAT1 = T ) ;hindi letter TT
( [ � � � ]   OCT340 OCT244 MAT1 = T h ) ;hindi letter TTH
( [ � � � ]   OCT340 OCT244 MAT1 = D ) ;hindi letter DD 
( [ � � � ]   OCT340 OCT244 MAT1 = D ) ;hindi letter DD U+095C DEVANAGARI LETTER DDDHA (U+0921 DEVANAGARI LETTER DDA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT244 MAT1 = D h ) ;hindi letter DDH 
( [ � � � ]   OCT340 OCT244 MAT1 = D h ) ;hindi letter DDH U+095D DEVANAGARI LETTER RHA (U+0922 DEVANAGARI LETTER DDHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT244 MAT1 = N ) ;hindi letter NN
( [ � � � ]   OCT340 OCT244 MAT1 = th ) ;hindi letter T
( [ � � � ]  OCT340 OCT244 MAT1  = tth ) ;hindi letter TH
( [ � � � ]   OCT340 OCT244 MAT1 = dh ) ;hindi letter D
( [ � � � ]   OCT340 OCT244 MAT1 = ddh ) ;hindi letter DH
( [ � � � ]   OCT340 OCT244 MAT1 = n ) ;hindi letter N
( [ � � � ]   OCT340 OCT244 MAT1 = n ) ;hindi letter N
( [ � � � ]   OCT340 OCT244 MAT1 = p ) ;hindi letter P
( [ � � � ]   OCT340 OCT244 MAT1 = f ) ;hindi letter PH 
( [ � � � ]   OCT340 OCT244 MAT1 = f ) ;hindi letter PH U+095F DEVANAGARI LETTER YYA (U+092F DEVANAGARI LETTER YA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT244 MAT1 = b ) ;hindi letter B
( [ � � � ]   OCT340 OCT244 MAT1 = bh ) ;hindi letter BH
( [ � � � ]   OCT340 OCT244 MAT1 = m ) ;hindi letter M
( [ � � � ]   OCT340 OCT244 MAT1 = y ) ;hindi letter Y 
( [ � � � ]   OCT340 OCT244 MAT1 = y ) ;hindi letter Y U+095F DEVANAGARI LETTER YYA (U+092F DEVANAGARI LETTER YA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT244 MAT1 = r ) ;hindi letter R
( [ � � � ] OCT340 OCT244 MAT1 = r ) ;hindi letter RR
( [ � � � ]  OCT340 OCT244 MAT1  = l) ;hindi letter L
( [ � � � ]   OCT340 OCT244 MAT1 = L ) ;hindi letter LL
( [ � � � ]   OCT340 OCT244 MAT1 = L ) ;hindi letter LLLA, U+0934 DEVANAGARI LETTER LLLA (U+0933 DEVANAGARI LETTER LLA + U+093C DEVANAGARI SIGN NUKTA) for transcribing Dravidian l
( [ � � � ]   OCT340 OCT244 MAT1 = v ) ;hindi letter V
( [ � � � ]   OCT340 OCT244 MAT1 = sh ) ;hindi letter SH
( [ � � � ]   OCT340 OCT244 MAT1 = sh h ) ;hindi letter SS
( [ � � � ]   OCT340 OCT244 MAT1 = s ) ;hindi letter S
( [ � � � ]   OCT340 OCT244 MAT1 = h ) ;hindi letter H
( [ � � � ]   OCT340 OCT245 MAT2  =  k ) ;hindi letter K
( [ � � � ]  OCT340 OCT245 MAT2  =  k ) ;hindi letter K U+0958 DEVANAGARI LETTER QA ( U+0915 DEVANAGARI LETTER KA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]  OCT340 OCT245 MAT2   = kh ) ;hindi letter KH
( [ � � � ]  OCT340 OCT245 MAT2 = kh ) ;hindi letter KH U+0959 DEVANAGARI LETTER KHHA (U+0916 DEVANAGARI LETTER KHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]  OCT340 OCT245 MAT2   = g ) ;hindi letter G
( [ � � � ]   OCT340 OCT245 MAT2 = g ) ;hindi letter G U+095A DEVANAGARI LETTER GHHA (U+0917 DEVANAGARI LETTER GA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT245 MAT2  = g h ) ;hindi letter GH
( [ � � � ]   OCT340 OCT245 MAT2  = n y ) ;hindi letter NG
( [ � � � ]   OCT340 OCT245 MAT2  = ch ) ;hindi letter C
( [ � � � ]  OCT340 OCT245 MAT2  = ch h ) ;hindi letter CH
( [ � � � ]  OCT340 OCT245 MAT2  = j ) ;hindi letter J
( [ � � � ]  OCT340 OCT245 MAT2  = j ) ;hindi letter J U+095B DEVANAGARI LETTER ZA ( U+091C DEVANAGARI LETTER JA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT245 MAT2  = j h ) ;hindi letter JH
( [ � � � ]   OCT340 OCT245 MAT2  = eh n eh ) ;hindi letter NY
( [ � � � ]  OCT340 OCT245 MAT2   = T ) ;hindi letter TT
( [ � � � ]   OCT340 OCT245 MAT2  = T h ) ;hindi letter TTH
( [ � � � ]   OCT340 OCT245 MAT2  = D ) ;hindi letter DD
( [ � � � ]   OCT340 OCT245 MAT2 = D ) ;hindi letter DD U+095C DEVANAGARI LETTER DDDHA (U+0921 DEVANAGARI LETTER DDA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT245 MAT2  = D h ) ;hindi letter DDH
( [ � � � ]   OCT340 OCT245 MAT2 = D h ) ;hindi letter DDH U+095D DEVANAGARI LETTER RHA (U+0922 DEVANAGARI LETTER DDHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]  OCT340 OCT245 MAT2   = N ) ;hindi letter NN
( [ � � � ]   OCT340 OCT245 MAT2  = th ) ;hindi letter T
( [ � � � ]  OCT340 OCT245 MAT2   = tth ) ;hindi letter TH
( [ � � � ]   OCT340 OCT245 MAT2  = dh ) ;hindi letter D
( [ � � � ]   OCT340 OCT245 MAT2  = ddh ) ;hindi letter DH
( [ � � � ]   OCT340 OCT245 MAT2  = n ) ;hindi letter N
( [ � � � ]   OCT340 OCT245 MAT2  = n ) ;hindi letter N
( [ � � � ]   OCT340 OCT245 MAT2  = p ) ;hindi letter P
( [ � � � ]   OCT340 OCT245 MAT2  = f ) ;hindi letter PH
( [ � � � ]   OCT340 OCT244 MAT2 = f ) ;hindi letter PH U+095F DEVANAGARI LETTER YYA (U+092F DEVANAGARI LETTER YA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]   OCT340 OCT245 MAT2  = b ) ;hindi letter B
( [ � � � ]   OCT340 OCT245 MAT2  = bh ) ;hindi letter BH
( [ � � � ]   OCT340 OCT245 MAT2  = m ) ;hindi letter M
( [ � � � ]   OCT340 OCT245 MAT2  = y ) ;hindi letter Y
( [ � � � ]   OCT340 OCT245 MAT2 = y ) ;hindi letter Y U+095F DEVANAGARI LETTER YYA (U+092F DEVANAGARI LETTER YA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ]  OCT340 OCT245 MAT2   = r ) ;hindi letter R
([ � � � ] OCT340 OCT245 MAT2  = r ) ;hindi letter RR
( [ � � � ]   OCT340 OCT245 MAT2  = l) ;hindi letter L
( [ � � � ]  OCT340 OCT245 MAT2   = L ) ;hindi letter LL
( [ � � � ]   OCT340 OCT245 MAT2 = L ) ;hindi letter LLLA, U+0934 DEVANAGARI LETTER LLLA (U+0933 DEVANAGARI LETTER LLA + U+093C DEVANAGARI SIGN NUKTA) for transcribing Dravidian l
( [ � � � ]   OCT340 OCT245 MAT2  = v ) ;hindi letter V
( [ � � � ]   OCT340 OCT245 MAT2  = sh ) ;hindi letter SH
( [ � � � ]   OCT340 OCT245 MAT2  = sh h ) ;hindi letter SS
( [ � � � ]  OCT340 OCT245 MAT2   = s ) ;hindi letter S
( [ � � � ]   OCT340 OCT245 MAT2  = h ) ;hindi letter H
;consonants
( [ � � � ]   =  k a ) ;hindi letter KA
( [ � � � ]  =  k a ) ;hindi letter KA U+0958 DEVANAGARI LETTER QA ( U+0915 DEVANAGARI LETTER KA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ] = kh a ) ;hindi letter KHA
( [ � � � ] = kh a ) ;hindi letter KHA +0959 DEVANAGARI LETTER KHHA (U+0916 DEVANAGARI LETTER KHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ] = g a ) ;hindi letter GA
( [ � � � ] = g a ) ;hindi letter GA U+095A DEVANAGARI LETTER GHHA (U+0917 DEVANAGARI LETTER GA + U+093C DEVANAGARI SIGN NUKTA) 
( [ � � � ] = g h a ) ;hindi letter GHA
( [ � � � ] = n y a ) ;hindi letter NGA
( [ � � � ] = ch a ) ;hindi letter CA
( [ � � � ] = ch h a ) ;hindi letter CHA
( [ � � � ] = j a ) ;hindi letter JA
( [ � � � ] = j a ) ;hindi letter JA U+095B DEVANAGARI LETTER ZA ( U+091C DEVANAGARI LETTER JA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ] = j h a ) ;hindi letter JHA
( [ � � � ] = eh n eh a ) ;hindi letter NYA
( [ � � � ] = T a ) ;hindi letter TTA
( [ � � � ] = T h a ) ;hindi letter TTHA
( [ � � � ] = D a ) ;hindi letter DDA 
( [ � � � ] = D a ) ;hindi letter DDA U+095C DEVANAGARI LETTER DDDHA (U+0921 DEVANAGARI LETTER DDA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ] = D h a ) ;hindi letter DDHA
( [ � � � ] = D h a ) ; hindi letter DDHA U+095D DEVANAGARI LETTER RHA (U+0922 DEVANAGARI LETTER DDHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ] = N a ) ;hindi letter NNA
( [ � � � ] = th a ) ;hindi letter TA
( [ � � � ] = tth a ) ;hindi letter THA
( [ � � � ] = dh a ) ;hindi letter D
( [ � � � ] = ddh a ) ;hindi letter DHA
( [ � � � ] = n a ) ;hindi letter NA
( [ � � � ] = n a ) ;indi letter NA U+0928 DEVANAGARI LETTER NA + U+093C DEVANAGARI SIGN NUKTA
( [ � � � ] = p a ) ;hindi letter PA
( [ � � � ] = f a ) ;hindi letter PHA
( [ � � � ] = f a ) ;hindi letter PHA U+095E DEVANAGARI LETTER FA (U+092B DEVANAGARI LETTER PHA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ] = b a ) ;hindi letter BA
( [ � � � ] = bh a ) ;hindi letter BHA
( [ � � � ] = m a ) ;hindi letter MA
( [ � � � ] = y a ) ;hindi letter YA
( [ � � � ] = y a ) ;hindi letter YA U+095F DEVANAGARI LETTER YYA (U+092F DEVANAGARI LETTER YA + U+093C DEVANAGARI SIGN NUKTA)
( [ � � � ] = r a ) ;hindi letter RA
( [ � � � ] = r a ) ;hindi letter RRA
( [ � � � ] = l a ) ;hindi letter LA
( [ � � � ] = L a ) ;hindi letter LLA
( [ � � � ] = L a ) ;hindi letter LLA U+0934 DEVANAGARI LETTER LLLA (U+0933 DEVANAGARI LETTER LLA + U+093C DEVANAGARI SIGN NUKTA) for transcribing Dravidian l
( [ � � � ] = v a ) ;hindi letter VA
( [ � � � ] = sh a ) ;hindi letter SHA
( [ � � � ] = sh h a ) ;hindi letter SSA
( [ � � � ] = s a ) ;hindi letter SA
( [ � � � ] = h a ) ;hindi letter HA
( [ � � � ] = oh m) ;U+0950 DEVANAGARI OM 
;;dependent vowels
( [ � � � ] = ay ) ;hindi vowel sign AI 0C46 (E)+ 0C56(AI Length Mark)
( [ � � � ] =  aa )  ;hindi vowel sign AA
( [ � � � ] =  ih ) ;hindi vowel sign I
( [ � � � ] = iy ) ;hindi vowel sign II
( [ � � � ] = uh ) ;hindi vowel sign U
( [ � � � ] = uw ) ;hindi vowel sign UU
( [ � � � ] = r eh )  ;hindi vowel sign vocalic R
( [ � � � ] = r ee )  ;hindi vowel sign vocalic RR
( [ � � � ] = ee ) ;hindi vowel sign chandra E
( [ � � � ] = n ) ;hindi U+0901 DEVANAGARI SIGN CANDRABINDU
( [ � � � ] = eh ) ;hindi vowel sign short E (for transcribing Dravidian vowels)
( [ � � � ] = eh ) ;hindi vowel sign E
( [ � � � ] = ay ) ;hindi vowel sign AI
( [ � � � ] = oo ) ;hindi vowel sign chandra O
( [ � � � ] = oh ) ;U+094A DEVANAGARI VOWEL SIGN SHORT O (for transcribing Dravidian vowels)
( [ � � � ] = oh ) ;hindi vowel sign O
( [ � � � ] = aw ) ;hindi vowel sign AU
;( [ � � � ]= )  ;ignoring nukta sign U+093C
;( [ � � � ] = ) ;ignoring U+093D DEVANAGARI SIGN AVAGRAHA
( [ � � � ] = ) ;ignoring U+094D DEVANAGARI SIGN VIRAMA, halant (the preferred Hindi name)

   )
)

;;; Lexicon definition
(lex.create "hindi")
(lex.set.phoneset "hindi")

(define (hindi_lts_function word features)
  "(hindi_lts_function WORD FEATURES)
 Using letter to sound rules to bulid a hindi pronunciation of WORD."
   (list word
	 nil
          (lex.syllabify.phstress (lts.apply (downcase word) 'hindi))))
(lex.set.lts.method 'hindi_lts_function)
(hindi_addenda)

(provide 'hindi_lex)


